﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerReview.Infrastructure
{
    public class ProjectSession
    {
        public static string ShortName
        {
            get
            {
                if (HttpContext.Current.Session["shortName"] == null)
                {
                    return string.Empty;
                }
                return (string)HttpContext.Current.Session["shortName"];
            }
            set
            {
                HttpContext.Current.Session["shortName"] = value;
            }
        }


        public static int OfficeId
        {
            get
            {
                if (HttpContext.Current.Session["officeId"] == null)
                {
                    return 0;
                }
                return (int)HttpContext.Current.Session["officeId"];
            }
            set
            {
                HttpContext.Current.Session["officeId"] = value;
            }
        }


        public static byte Rating
        {
            get
            {
                if (HttpContext.Current.Session["rating"] == null)
                {
                    return 0;
                }
                return (byte)HttpContext.Current.Session["rating"];
            }
            set
            {
                HttpContext.Current.Session["rating"] = value;
            }
        }

        public static bool IsAllowCaptcha
        {
            get
            {
                if (HttpContext.Current.Session["isallowcaptcha"] == null)
                {
                    return false;
                }
                return (bool)HttpContext.Current.Session["isallowcaptcha"];
            }
            set
            {
                HttpContext.Current.Session["isallowcaptcha"] = value;

            }
        }
    }
}