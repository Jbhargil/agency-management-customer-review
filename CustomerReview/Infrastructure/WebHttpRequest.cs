﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Text;
using System.Web.Mvc;
using CustomerReview.Models;
using Newtonsoft.Json;

namespace CustomerReview.Infrastructure
{

    public class WebHttpRequest
    {
        public string Request(string url, string data, string method = "POST", string contentType = "application/json")
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(ConstastVariable.BaseUrl + url);
                request.Method = method;
                request.ContentType = contentType;
                
                if (!string.IsNullOrWhiteSpace(data))
                {
                    byte[] postdata = Encoding.ASCII.GetBytes(data);
                    request.ContentLength = postdata.Length;
                    using (Stream stream = request.GetRequestStream())
                    {
                        stream.Write(postdata, 0, postdata.Length);
                    }
                }
                else
                {
                    request.ContentLength = 0;
                }
               
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                ContentResult cr = new ContentResult();
                cr.Content = responseString;
                return cr.Content;
            }
            
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}