﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CustomerReview.Models;

namespace CustomerReview.Infrastructure
{
    public class ResponceMethod<T>
    {

        public static ResponseModel<string> ErrorResponse(int statusCode, string errorMessage)
        {
            ResponseModel<string> obj = new ResponseModel<string>();
            obj.Status = statusCode;
            obj.Message = errorMessage;
            obj.Data = null;
            return obj;
        }
        public static ResponseModel<T> SuccessResponse(int status, string message, T ResponseModel)
        {
            ResponseModel<T> response = new ResponseModel<T>();
            response.Status = status;
            response.Message = message;
            response.Data = ResponseModel;
            return response;
        }

        public static ResponseListModel<T> SuccessResponse(int status, string message, List<T> ResponseModel)
        {
            ResponseListModel<T> response = new ResponseListModel<T>();
            response.Status = status;
            response.Message = message;
            response.Data = ResponseModel;
            return response;
        }
    }
}