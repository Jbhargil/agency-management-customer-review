﻿using CustomerReview.Infrastructure;
using CustomerReview.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CustomerReview.Controllers
{
    public class SocialMediaFormController : Controller
    {
        public PartialViewResult StarSocialreviewlink(int officeId)
        {
            WebHttpRequest web = new WebHttpRequest();
            officeId = Convert.ToInt32(System.Web.HttpContext.Current.Session["officeId"].ToString());
            var content = web.Request("review/social-media/" + officeId + "/all", null, "GET");
            OfficeSocialMediaTypeModel model = new OfficeSocialMediaTypeModel();

            var splashinfo = JsonConvert.DeserializeObject<ResponseListModel<OfficeSocialMediaTypeModel>>(content);
            return PartialView("~/Views/SocialMediaForm/Partial/_StarSociallinkReview.cshtml", splashinfo.Data);
        }


        public PartialViewResult Customerfeedback(string rating)
        {
            CustomerReviewModel model = new CustomerReviewModel();
            model.Rating = Convert.ToDecimal(rating);
            return PartialView("~/Views/SocialMediaForm/Partial/_StarLinkReview.cshtml", model);
        }

        public ActionResult PostCustomerfeedback(CustomerReviewModel model)
        {
            WebHttpRequest web = new WebHttpRequest();
            CustomerReviewModel customerreview = new CustomerReviewModel();
            customerreview.OfficeId = Convert.ToInt32(System.Web.HttpContext.Current.Session["officeId"].ToString());
            customerreview.FirstName = model.FirstName;
            customerreview.LastName = model.LastName;
            customerreview.MessagesText = model.MessagesText;
            customerreview.Email = model.Email;
            customerreview.ContactNo = model.ContactNo;
            customerreview.Description = model.Description;
            customerreview.Rating = model.Rating;

            JavaScriptSerializer js = new JavaScriptSerializer();
            string jsonData = js.Serialize(customerreview);
            var content = web.Request("customer-review", jsonData, "POST");
            return RedirectToAction("Index", "CustomerReview", new { id = ProjectSession.ShortName });
        }

        [Route("~/error-404")]
        public ActionResult Error404()
        {
            return View();
        }
    }
}