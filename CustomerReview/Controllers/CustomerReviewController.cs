﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerReview.Infrastructure;
using CustomerReview.App_Start;
using Newtonsoft.Json;
using CustomerReview.Models;


namespace CustomerReview.Controllers
{
    public class CustomerReviewController : Controller
    {
        public ActionResult Index(string id)
        {
            WebHttpRequest web = new WebHttpRequest();
            if (!string.IsNullOrEmpty(id))
            {
                var content = web.Request("review/office-content/s/" + id, null, "GET");
                
                OfficeContentModel model = new OfficeContentModel();
                var splashInfo = JsonConvert.DeserializeObject<ResponseModel<OfficeContentModel>>(content);
                if (splashInfo.Data != null)
                {
                    ProjectSession.ShortName = id;
                    model.ShortName = ProjectSession.ShortName;
                    model.ReviewStarLimit = splashInfo.Data.ReviewStarLimit;
                    model.BodyText = splashInfo.Data.BodyText;
                    model.Logo = splashInfo.Data.Logo;
                    model.OfficeId = splashInfo.Data.OfficeId;

                    model.IsAllowCaptcha = splashInfo.Data.IsAllowCaptcha;
                    
                    ProjectSession.IsAllowCaptcha = model.IsAllowCaptcha;
                    ProjectSession.OfficeId = model.OfficeId;
                    ProjectSession.Rating = model.ReviewStarLimit;
                    int officeId = ProjectSession.OfficeId;
                    decimal rating = ProjectSession.Rating;
                    return View(splashInfo.Data);
                }
            }
            return RedirectToAction("Error404","SocialMediaForm");
        }


    }
}