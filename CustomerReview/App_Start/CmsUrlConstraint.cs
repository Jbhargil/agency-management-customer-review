﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace CustomerReview.App_Start
{
    public class CmsUrlConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            string permalink = values[parameterName].ToString();
            HttpContext.Current.Items["cmspage"] = permalink;
            return true;
        }
    }
}