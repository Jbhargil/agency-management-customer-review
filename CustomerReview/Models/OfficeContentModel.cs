﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CustomerReview.Models
{
    public class OfficeContentModel
    {
        [JsonProperty("officeId")]
        public int OfficeId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }


        [JsonProperty("shortName")]
        public string ShortName { get; set; }

        [JsonProperty("preFaceText")]
        public string PreFaceText { get; set; }

        [JsonProperty("bodyText")]
        public string BodyText { get; set; }

        [JsonProperty("reviewStarLimit")]
        public byte ReviewStarLimit { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("isAllowCaptcha")]
        public bool IsAllowCaptcha { get; set; }
    }
}