﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace CustomerReview.Models
{
    public class CustomerReviewModel
    {
        [JsonProperty("customerReviewId")]
        public long CustomerReviewId { get; set; }

        [JsonProperty("officeId")]
        public int OfficeId { get; set; }

        [JsonProperty("socialMediaTypeId")]
        public int? SocialMediaTypeId { get; set; }

        [JsonProperty("firstName")]
        [Required(ErrorMessage = "Enter FirstName.")]
        [DisplayName("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        [Required(ErrorMessage = "Enter LastName.")]
        [DisplayName("LastName")]
        public string LastName { get; set; }

        [JsonProperty("contactNo")]
        [Required(ErrorMessage = "Enter ContactNo.")]
        [DisplayName("ContactNo")]
        public string ContactNo { get; set; }

        [JsonProperty("email")]
        [Required(ErrorMessage = "Enter Email.")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [JsonProperty("messagesText")]
        [Required(ErrorMessage = "Enter MessagesText.")]
        [DisplayName("MessagesText")]
        public string MessagesText { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("createdById")]
        public long CreatedById { get; set; }

        [JsonProperty("modifiedDate")]
        public DateTime ModifiedDate { get; set; }

        [JsonProperty("modifiedById")]
        public long? ModifiedById { get; set; }

        [JsonProperty("customerId")]
        public long? CustomerId { get; set; }

        [JsonProperty("profileUrl")]
        public string ProfileUrl { get; set; }

        [JsonProperty("userPic")]
        public string UserPic { get; set; }

        [JsonProperty("rating")]
        public decimal? Rating { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }


    }
}