﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerReview.Models
{
  
        public class ResponseModel<T>
        {
            public int Status { get; set; }
            public string Message { get; set; }
            public T Data { get; set; }
        }

        public class ResponseListModel<T>
        {
            public int Status { get; set; }
            public string Message { get; set; }
            public List<T> Data { get; set; }
        }
    
}