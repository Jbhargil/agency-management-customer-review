﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;


namespace CustomerReview.Models
{
    public class OfficeSocialMediaTypeModel
    {
        [JsonProperty("officeSocialMediaTypeId")]
        public long OfficeSocialMediaTypeId { get; set; }
        [JsonProperty("socialMediaTypeId")]
        public int SocialMediaTypeId { get; set; }
        [JsonProperty("socialMediaName")]
        public string SocialMediaName { get; set; }
        [JsonProperty("socialMediaIconURL")]
        public string SocialMediaIconURL { get; set; }
        [JsonProperty("socialMediaCode")]
        public string SocialMediaCode { get; set; }
        [JsonProperty("officeId")]
        public int OfficeId { get; set; }
        [JsonProperty("redirectionURL")]
        public string RedirectionURL { get; set; }
    }
}